/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.architecture.blueprints.todoapp.tasks;

import android.support.annotation.NonNull;

import com.example.android.architecture.blueprints.todoapp.BaseView;
import com.example.android.architecture.blueprints.todoapp.data.Task;
import com.example.android.architecture.blueprints.todoapp.BasePresenter;

import java.util.List;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface TasksContract {

    interface View extends BaseView<Presenter> {
        //设置加载指示器
        void setLoadingIndicator(boolean active);

        //显示任务列表
        void showTasks(List<Task> tasks);

        //跳转到新增任务
        void showAddTask();

        //跳转到任务详情界面
        void showTaskDetailsUi(String taskId);

        //显示任务已完成
        void showTaskMarkedComplete();

        //显示任务未完成
        void showTaskMarkedActive();

        //提示任务被清理
        void showCompletedTasksCleared();

        //提示加载任务失败
        void showLoadingTasksError();

        //显示没有任务界面
        void showNoTasks();

        //显示未激活的标签
        void showActiveFilterLabel();

        //显示已完成的标签
        void showCompletedFilterLabel();

        //显示所有的标签
        void showAllFilterLabel();

        //显示没有未完成的任务内容提示
        void showNoActiveTasks();

        //显示没有已完成的任务内容提示
        void showNoCompletedTasks();

        //显示成功添加任务
        void showSuccessfullySavedMessage();

        //返回fragment的isAdd()?
        boolean isActive();

        //显示任务分类的pop
        void showFilteringPopUpMenu();
    }

    interface Presenter extends BasePresenter {
        //If a task was successfully added, show snackbar 添加成功的判断,使用requestCode和resultCode的判断
        void result(int requestCode, int resultCode);

        //加载任务框(直接调用本地数据库里的内容进行处理)
        void loadTasks(boolean forceUpdate);

        //添加新任务
        void addNewTask();

        //非空判断之后,调用view的显示任务接口(与view的桥接)
        void openTaskDetails(@NonNull Task requestedTask);

        //将任务标记完成并且调用view的显示完成接口,加载任务
        void completeTask(@NonNull Task completedTask);

        //将任务标记为未完成,执行逻辑,加载任务
        void activateTask(@NonNull Task activeTask);

        //清除任务,执行逻辑,更新UI
        void clearCompletedTasks();

        //设置任务过滤类型
        void setFiltering(TasksFilterType requestType);

        //获取任务过滤类型
        TasksFilterType getFiltering();
    }
}
